import env from 'dotenv';
import Koa from 'koa';
import logger from 'koa-morgan';
import bodyParser from 'koa-bodyparser';
import cors from '@koa/cors';

env.config();

const port = process.env.PORT || '4444';

const app: Koa = new Koa();

import Router from 'koa-router';

export const routers = new Router();

routers.get('/', async ctx => {
  ctx.body = `Ready ! =)`;
});

app
  .use(logger('tiny'))
  .use(cors())
  .use(routers.routes())
  .use(routers.allowedMethods())
  .use(bodyParser())
  .use(async (ctx: Koa.Context) => {
    ctx.body = 'Hello world';
  })
  .listen(port, () => {
    console.log(`>>> 🌍 >>> Server running on port >>> ${port}`);
  });
