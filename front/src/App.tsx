import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>

        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer">
          Learn React
        </a>

        <button className="px-4 py-2 rounded-lg bg-cyan-800 text-cyan-400 hover:bg-cyan-700 hover:text-cyan-100">
          btn
        </button>
      </header>
    </div>
  );
}

export default App;
